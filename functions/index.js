const functions = require('firebase-functions');
const nodemailer = require('nodemailer');
const htmlToText = require('nodemailer-html-to-text').htmlToText;
const {email, password} = require('./config');
const mailTransport = nodemailer.createTransport({
    service: 'gmail',
    auth: {
        user: email,
        pass: password
    }
});
const formatPrice = function (price) {
    return price.toLocaleString('en-US', {
        style: 'currency',
        currency: 'USD'
    })
};

mailTransport.use('compile', htmlToText());

// // Create and Deploy Your First Cloud Functions
// // https://firebase.google.com/docs/functions/write-firebase-functions
//

const APP_NAME = 'orderSys';

exports.sendUserEmail = functions.database
    .ref("/orders/{pushId}")
    .onCreate(order => {
        console.log('Sending User Email...');
        sendOrderEmail(order.val());
    });

function sendOrderEmail(order) {
    const mailOptions = {
        from: `${APP_NAME} <noreply@ordersys.com`,
        to: order.email,
        subject: `Your Order from ${APP_NAME} is on the way to your address`,
        html: `
            <table style="width: 500px; margin: auto;">
                <tr>
                    <th style="background-color: #f44336; color: #fff;">${order.displayName}</th>
                    <th style="background-color: #f44336; color: #fff;">Item Description</th>
                    <th style="background-color: #f44336; color: #fff;">Price per quantity</th>
                    <th style="background-color: #f44336; color: #fff;">Total Price</th>
                </tr>
                ${order.order.map(({name, quantity, price}) => `
                    <tr>
                        <td style="text-align: center;">
                            ${quantity}
                        </td>
                        <td>
                            ${name}
                        </td>
                        <td style="text-align: center;">
                            ${formatPrice(price)}
                        </td>
                        <td style="text-align: center;">
                            ${formatPrice((price * quantity))}
                        </td>
                    </tr>
                `).join("")}
                <tfoot>
                    <tr>
                        <td>${order.total}</td>
                    </tr>
                </tfoot>
            </table>
        `
    };
    mailTransport.sendMail(mailOptions);
}
