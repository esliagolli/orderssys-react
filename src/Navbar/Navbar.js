import React from 'react';
import styled from 'styled-components';
import {pizzaRed} from "../Styles/colors";
import {Title} from "../Styles/title";

const NavbarStyled = styled.div`
    background-color: ${pizzaRed};
    padding: 10px;
    position: fixed;
    width: 100%;
    z-index: 999;
    display: flex;
    justify-content: space-between;
`;

const Logo = styled(Title)`
    font-size: 20px;
    color: white;
    text-shadow: 1px 1px 4px #380502;
`;

const UserStatus = styled.div`
    font-size: 12px;
    color: white;
    margin-right: 30px;
`;

const LoginButton = styled.span`
    cursor: pointer;
`;

export function Navbar({login, loggedIn, logout}) {
    const profPic = loggedIn ? <img style={{width: '25px', borderRadius: '50%'}} src={loggedIn.photoURL}/> : '';

    return <NavbarStyled>
        <Logo>
            SliceLine <span role="img" aria-label="pizza slice">🍕</span>
        </Logo>
        <UserStatus>
            {loggedIn !== 'loading' ? (
                <>
                    {loggedIn ? `Hello ${loggedIn.displayName}` : ""}
                    {loggedIn ? profPic : ''}
                    {loggedIn ? (
                        <LoginButton onClick={logout}>Log Out</LoginButton>
                    ) : (
                        <LoginButton onClick={login}>Log in / Sign Up</LoginButton>
                    )}
                </>
            ) : (
                "loading..."
            )}
        </UserStatus>
    </NavbarStyled>;
}